#include "../TrigMuonEFTrackIsolationTool.h"
#include "../TrigMuonEFTrackIsolationAlgMT.h"
#include "../MuonFilterAlg.h"
#include "../MuonChainFilterAlg.h"
#include "../MergeEFMuonsAlg.h"

DECLARE_COMPONENT( TrigMuonEFTrackIsolationTool )
DECLARE_COMPONENT( TrigMuonEFTrackIsolationAlgMT )
DECLARE_COMPONENT( MuonFilterAlg )
DECLARE_COMPONENT( MuonChainFilterAlg )
DECLARE_COMPONENT( MergeEFMuonsAlg )

