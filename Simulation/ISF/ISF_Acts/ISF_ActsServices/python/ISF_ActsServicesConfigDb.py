# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration


from AthenaCommon.CfgGetter import addTool

addTool("ISF_ActsServices.ISF_ActsServicesConfig.getActsSimServiceID",  "ISF_ActsSimSvc")
addTool("ISF_ActsServices.ISF_ActsServicesConfig.getActsSimulatorToolST", "ISF_ActsSimulatorToolST")
